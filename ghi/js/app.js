function createCard(confName, confDescription, pictureUrl, locationName, startDate, endsDate) {
  return `
    <div class="col-4">
			<div class="card shadow-sm mb-2">
				<img src="${pictureUrl}" class="card-img-top">
				<div class="card-body">
					<h5 class="card-title">${confName}</h5>
					<h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
					<p class="card-text">${confDescription}</p>
				</div>
			</div>
			<div class="card-footer">
				From: ${startDate} - To: ${endsDate}
			</div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
				console.error("Bad Response!")

      } else {
        const data = await response.json(); // make JS
  
				for (let conference of data.conferences) {
					const detailUrl = `http://localhost:8000${conference.href}`;
					const detailResponse = await fetch(detailUrl);
					if (detailResponse.ok) {
						const details = await detailResponse.json();
						const confName = details.conference.name;
						const confDescription = details.conference.description;
						const pictureUrl = details.conference.location.picture_url;
						const locationName = details.conference.location.name;
						const starts = new Date(details.conference.starts);
						const ends = new Date(details.conference.ends);
						const startDate = starts.toLocaleDateString();
						const endDate = ends.toLocaleDateString();
						const html = createCard(confName, confDescription, pictureUrl,locationName, startDate, endDate);
						const column = document.querySelector('.row');
						column.innerHTML += html;
					}
				}
  
      }
    } catch (e) {
			console.error();
      const alert = document.querySelector("#error")
			alert.innerHTML += 
				`<div class="alert alert-warning" role="alert">
				***** API URL RETRIEVAL FAIED!!! *****
				</div>`
    }
  });




